Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

#Bug
#Product name is case sensitive which is wrong. If I enter url like  https://waarkoop-server.herokuapp.com/api/v1/search/test/Mango instead of mango, then error is being thrown.

 Scenario:
    Given client calls endpoint PRODUCT_API for "mango"
    Then for "mango" product endpoint, the client should receive an HTTP 200 response code
     
 Scenario:
	 Given client tries to fetch product with any invalid "<script>" name
	 Then Error should be shown as product is invalid
	 And  for "<script>" status code should be "404"
   
 Scenario:
	 Given client tries to fetch "mango"
	 Then count of "mango" products for distributor "Vomar" should be 50
 