package starter.stepdefinitions;

import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.asynchttpclient.Response;
import org.junit.Assert;
import java.util.List;
import java.util.Map;

public class products {

static String PRODUCT_API = "https://waarkoop-server.herokuapp.com/api/v1/search/test/product";	
public static Response response;
public static void validate_get_api_status_code(String product, Integer code)
	{
		String Product = PRODUCT_API.replace("product", product);
		System.out.println("product url is "+Product);
	    SerenityRest.when().get(Product).then().assertThat().statusCode(code);
	    
	}


public static void get_product_count_as_per_distributor(String product, String distributor, Integer count) throws JsonMappingException, JsonProcessingException
{
	Object value = null;
	Double cnt = (double) 0;
	String Product = PRODUCT_API.replace("product", product);
	String response = SerenityRest.when().get(Product).then().contentType(ContentType.JSON).extract().response().asString(); 	
	List<Map<String, String>> response1 = (List<Map<String, String>>) new ObjectMapper().readValue(response , new TypeReference<List<Map<String, String>>>(){});
	
	for (int i=0; i < response1.size();i++)
	{
		Map<String,String> pair=((Map<String, String>) response1.get(i));
		value = pair.get("provider");
		if (value.toString().equalsIgnoreCase(distributor) == true)
			{
			cnt++;
		      }	
	}
final double DELTA = 1e-15;
Assert.assertEquals(count.doubleValue(), cnt.doubleValue(), DELTA);
}
}
