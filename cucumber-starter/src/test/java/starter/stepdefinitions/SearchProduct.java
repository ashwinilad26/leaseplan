package starter.stepdefinitions;

import static org.junit.Assert.assertTrue;

import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class SearchProduct {
	private Response response;

	@When("^client calls endpoint PRODUCT_API for \"([^\"]*)\"$")
	public void clientCallsEndpointFor(String arg1) throws Throwable {
	}
	
	@Then("^for \"([^\"]*)\" product endpoint, the client should receive an HTTP (\\d+) response code$")
	public void forProductEndpointTheClientShouldReceiveAnHTTPResponseCode(String arg1, int arg2) throws Throwable {
		products.validate_get_api_status_code(arg1, arg2);	
	}
	

	@Given("^client tries to fetch product with any invalid \"([^\"]*)\" name$")
	public void clientTriesToFetchProductWithAnyInvalidName(String arg1) throws Throwable {

	}

	@Then("^Error should be shown as product is invalid$")
	public void errorShouldBeShownAsProductIsInvalid() throws Throwable {

	}

	@And("^for \"([^\"]*)\" status code should be \"([^\"]*)\"$")
	public void forStatusCodeShouldBe(String arg1, int arg2) throws Throwable {
		products.validate_get_api_status_code(arg1, arg2);
	}

	@Given("^client tries to fetch \"([^\"]*)\"$")
	public void clientTriesToFetch(String arg1) throws Throwable {

	}


	@Then("^count of \"([^\"]*)\" products for distributor \"([^\"]*)\" should be (\\d+)$")
	public void countOfProductsForShouldBe(String arg1, String arg2, int arg3) throws Throwable {
		products.get_product_count_as_per_distributor(arg1, arg2, arg3);
	}
	
}
