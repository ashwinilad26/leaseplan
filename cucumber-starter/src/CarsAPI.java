package starter.stepdefinitions;

import java.util.Map;
import static java.util.stream.Collectors.toMap;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class CarsAPI {
	private static String PRODUCT_API = "https://waarkoop-server.herokuapp.com/api/v1/search/test/{product}";

    @Step("Get product details by product {0}")
    public void fetchProductDetails(String product) {
        SerenityRest.given()
                .pathParam("product", product)         
                .get(PRODUCT_API);
    }
}
