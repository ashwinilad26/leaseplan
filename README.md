# LeasePlan

Refactored/updated:

1. Changed the location of feature files.
2. Added glue = "starter.stepdefinitions" in TestRunner.java
3. Under starter.stepdefinitions 
  - deleted existing files
  - added 2 new files as product.java, SearchProduct.java(Step Definition)
  - added 3 methods under product.java which are being used in SearchProduct.java
4. Converted project to cucumber project.
